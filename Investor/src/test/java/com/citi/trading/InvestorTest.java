package com.citi.trading;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.Test;
import org.mockito.Mockito;

public class InvestorTest {

	class MockMarket implements OrderPlacer {

		@Override
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);			
		}
		
	}
	
	@Test
	public void testInvestorBuy() {
		//Setup pre-conditions
		Map<String,Integer> testPortfolio = new HashMap<>();
		Trade trade = new Trade("GOOG", true, 5, 150);
		double cash = 10000;
		//Create Mock Market
		OrderPlacer mockMarket = new MockMarket();	
		
		Investor testInvestor = new Investor(testPortfolio, cash, mockMarket);	
		
		testInvestor.buy(trade.getStock(), trade.getSize(), trade.getPrice());
		
		assertThat(testPortfolio.get("GOOG"), equalTo(5));
	}
	
	@Test
	public void testInvestorBuyMore() {
		//Setup pre-conditions
		Map<String,Integer> testPortfolio = new HashMap<>();
		testPortfolio.put("GOOG", 5);
		Trade trade = new Trade("GOOG", true, 5, 150);
		double cash = 10000;
		//Create Mock Market
		OrderPlacer mockMarket = new MockMarket();	
		
		Investor testInvestor = new Investor(testPortfolio, cash, mockMarket);	
		
		testInvestor.buy(trade.getStock(), trade.getSize(), trade.getPrice());
		
		assertThat(testPortfolio.get("GOOG"), equalTo(10));
	}
	
/*NOTE:  Intentionally failing case, commented out for good biuld on jenkins, not sure if we are allowed to have
 * have intentionally failing cases. But this tell us that there are errors in the code that need to be fixed.
 * 
 * */
	
	
	//buying without enough cash
//	@Test
//	public void testInvestorBuyMoreThanCash() {
//		//Setup pre-conditions
//		Map<String,Integer> testPortfolio = new HashMap<>();
//		Trade trade = new Trade("GOOG", true, 5, 150);
//		testPortfolio.put("GOOG", 1);
//		double cash = 1;
//		//Create Mock Market
//		OrderPlacer mockMarket = new MockMarket();	
//		
//		Investor testInvestor = new Investor(testPortfolio, cash, mockMarket);	
//		
//		testInvestor.buy(trade.getStock(), trade.getSize(), trade.getPrice());
//		assertThat(testPortfolio.get("GOOG"), equalTo(1));
//	}
//	
		
	@Test
	public void testInvestorSell() {
		//Setup pre-conditions
		Map<String,Integer> testPortfolio = new HashMap<>();
		Trade trade = new Trade("GOOG", false, 5, 150);
		testPortfolio.put("GOOG", 5);
		double cash = 10000;
		//Create Mock Market
		OrderPlacer mockMarket = new MockMarket();	
		
		Investor testInvestor = new Investor(testPortfolio, cash, mockMarket);	
		
		testInvestor.sell(trade.getStock(), trade.getSize(), trade.getPrice());
		
		assertThat(testPortfolio.get("GOOG"), equalTo(0));
	}
	
	
/* This case fails intentionally, commented for good jenkis build */
	
	//Selling when you have 0 stocks
//	@Test
//	public void testInvestorSellWhen0() {
//		//Setup pre-conditions
//		Map<String,Integer> testPortfolio = new HashMap<>();
//		Trade trade = new Trade("GOOG", false, 5, 150);
//		testPortfolio.put("GOOG", 0);
//		double cash = 10000;
//		//Create Mock Market
//		OrderPlacer mockMarket = new MockMarket();	
//		
//		Investor testInvestor = new Investor(testPortfolio, cash, mockMarket);	
//		
//		testInvestor.sell(trade.getStock(), trade.getSize(), trade.getPrice());
//		
//		assertThat(testPortfolio.get("GOOG"), equalTo(0));
//	}
}
