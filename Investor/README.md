# Testing Workshop

In this workshop exercise you will develop a unit test for a component that manages a portfolio of stock holdings. The **Investor** class holds a map of stock symbol (key) and shares owned (value), along with a cash balance; and offers methods that allow the caller to initiate stock trades, which then alter the portfolio (the map) and the cash balance.

The component relies on a second component, called **Market**, to place orders on a message queue, and to listen for notifications of trade confirmation (or e.g. rejection) on a separate queue. This poses a major obstacle to isolated unit testing, and you will need to develop a mock to replace this component in the testing environment.

## Criteria for Completed Work

Your project must be available on a public BitBucket repository, the URL of which you'll provide to the instructors for review. Implement as much of the exercise as you can, and whatever you have implemented must compile cleanly. The Maven 'test' goal must run cleanly as well.

All tests must use **assertThat()** and appropriate Hamcrest matchers.

You will also demonstrate a working Jenkins project that can run all tests on demand. Add the config.xml for the project to your code repository; this is unconventional, but it's harmless and gives us an easy way to transmit that one file without creating a second repository. The file is typically found in **/Users/<username>/.jenkins/jobs/<projectname>**.

## Setup

As you did in the previous workshop, create a new Eclipse/Maven/Git project, link to a BitBucket repository, and populate it with the starter code from this folder.

Notice that the project has a compile error. This is intentional -- a way of forcing one important code change before you can run the application. In **Market.java**, change the 'FirstNameLastName' part of the name of the ActiveMQ reply queue to your own first and last names, with no internal whitespace. Then remove the offending line of text so that the class compiles. The project as a whole will now build and run cleanly.

You can run **com.citi.trading.Investor** as a Java application, and the **main()** method will create three distinct instances of the class, each with different stock holdings and cash balance, and run a trade on each. You'll see the holdings and cash as of the completed trade. There is a short delay in getting trade confirmations from the remote "order broker" or mock market, so the **main()** method sleeps briefly after each trade request.

## Tasks

The overarching goal for this exercise is to get a solid unit test in place for this class. There are plenty of interesting test cases and code to cover. First however you will need to isolate the test from the externalities of remote message queues and message broker: i.e. you will need a "mock mock market."

1\. Add necessary dependencies to your **pom.xml** to support testing.

2\. Develop a mock that will take the place of the **Market** component. There are easier and harder ways to do this; think first about what you really need to mock, and remember that sometimes test development involves refactoring of the class under test.

3\. Develop a test class and one initial test case that requires your mock. This is probably the steepest part of the climb, and it proves out the mock that you've built, your ability to put it in play in favor of the original **Market** component, and that you can run your test in isolation, i.e. without triggering remote connections to ActiveMQ.

Push everything to your BitBucket repository at this point.

4\. Create a Jenkins project and bind it to your repository. Start a build and be sure that it runs 'mvn test' and succeeds.

5\. Add a second test case and get it running cleanly. Push your code, and be sure that Jenkins picks up the change when you run a new build.

6\. Build out test cases that cover the existing functionality of **Investor** to your satisfaction. Push and see clean Jenkins builds as you go along.

## Stretch Goals

1\. The component works well, as long as inputs are correct; but it does no input validation. What if it's asked to buy -500 shares of something -- or, more subtly, what if someone tries to buy shares they can't afford given their current cash? Improve the component's input validation, and try to do so in a test-driven way: for example start by developing a test that expects an **IllegalArgumentException** if a negative value is passed for the cash balance to the constructor. Watch that test fail, and __then__ improve the component so that the test succeeds. Then test another validation case, see that test fail, add code, test succeeds ... repeat.

2\. In a real market, we don't always get the trades that we request. The current mock market is running in a "perfect" mode such that it always fills your request, never any trouble. But other outcomes are possible: rejections, partial fills, and of course at a network level we might have a failure such that we never hear back from the market at all.

So this is difficult to integration-test, because we can't force the test conditions we want. Nice job for your mock component, though! Try enhancing your code such that an individual test case can set up a partial fill -- maybe you can only by half of the shares you wanted to buy? -- and then test that the **Investor** reacts to that correctly. Test rejected trades as well.

